---
title: Public Vulnerability Disclosure Policy
subtitle: How we try not to be jerks.
comments: false
version: 1.0
---

## Introduction

We at VidyaSEC are dedicated to protecting the global community from the effects of software and hardware vulnerabilities. In the course of investigation of malware, attack clients, and systems, we may find novel attack vectors in software or hardware publically sold by or distributed by individuals, businesses, or organizations (“Vendors”). In the interest of public safety, we distribute Advisories to the public to warn them of these vulnerabilities. However, we also understand that companies have their own restrictions and time constraints.

With that understanding in mind, we have implemented the following Policy to best balance the needs of the community and the company we are consulting. The policy outlined below was developed following industry best practices for responsible public disclosure of security vulnerabilities.

## Policy

VidyaSEC will responsibly notify vendors of security vulnerabilities in their products or services.

Regardless of vendor acceptance or rejection of the vulnerability’s validity, the vulnerability’s details will be released to the public in forty-five (45) days following completion of the Vulnerability Disclosure Process Control section outlined below.

All decisions related to the public disclosure of vulnerabilities lie solely with VidyaSEC. Unless extraordinary circumstances arise, the public release of a vulnerability by VidyaSEC will follow our standard procedure.

VidyaSEC will make every effort to help vendors understand how the vulnerability works in the interest of getting it patched. In the event a vendor cannot or will not patch a vulnerability, VidyaSEC will attempt to work with the vendor in publishing a workaround with the vulnerability details. In no case, however, will VidyaSEC suppress a vulnerability from being published.

VidyaSEC will not release vulnerability details publically without first contacting the vendor. VidyaSEC will internally vet the vulnerabilities before sending them to the vendor.

Communications between the vendor and VidyaSEC may be made public once the vulnerability itself has been made public. VidyaSEC will notify the vendor with publication plans. Alternate publication schedules can be made at the discretion of VidyaSEC.

In the event a vendor does not respond, or refuses, is unable to, or makes unreasonable demands of remediation, VidyaSEC reserves the right to publically disclose security vulnerabilities within 15 business days of the initial notification, regardless of the existence or availability of fixes, patches, or other remediation. The final schedule of publication lies solely at VidyaSEC’s discretion.

## Controls and Process

1. Vulnerabilities discovered by VidyaSEC have been identified by qualified security engineers and analyzed by our review board.
1. Upon discovery, we will check known public reporting databases for any duplicate reports.
1. Upon discovery, VidyaSEC’s first initial contact will be made through any appropriate contacts listed on the company’s website, or by sending an email to the appropriate point of contact (security@, support@, info@, secure@vendor.com, etc), with the pertinent information about the vulnerability. VidyaSEC will not use online forms to submit vulnerability information. However, VidyaSEC may use an online form to request a security point of contact. VidyaSEC will PGP-encrypt all emails exchanged with the vendor, provided vendor supports PGP and can provide a public key. During this initial email contact, VidyaSEC will notify vendor of its intention to publicate the vulnerability, and on which timeline. The vendor is encouraged to reply to the email and work with VidyaSEC to determine a resolution timeframe.
1. If the vendor fails to respond to the initial VidyaSEC contact email within five (5) business days, VidyaSEC will attempt to formally contact a representative at the vendor a second time. Should the vendor fail to respond for another five (5) business days, VidyaSEC may rely on an intermediary to establish contact. If VidyaSEC exhausts all possible means to contact the vendor, VidyaSEC will release the report after fifteen (15) days after the initial contact attempt.
1. In the event of a sufficiently severe breach (such as breach of personal information), VidyaSEC reserves the right to contact Carnegie Mellon’s Computer Emergency Response Team (CERT) or US-CERT, regardless of whether the vendor has responded to VidyaSEC or not.
1. VidyaSEC realizes that some issues may take longer to resolve than the allotted timeframe, and we are willing to work with vendors in resolving the problem within a reasonable time frame. If the vendor is unresponsive, unable, or unwilling to resolve the problem within a reasonable time frame without an acceptable statement as to why, VidyaSEC reserves the right to publish vulnerability details to the defensive community with or without notification to the vendor. VidyaSEC may grant additional time upon request, but will require proactive, regular updates regarding progress. Updates must be delivered within a month interval. If an expected update is not received, VidyaSEC will make three (3) attempts at contact before publishing the vulnerability.

## Policy Management

VidyaSEC reserves the right to change this Policy as needed, with or without notification to vendors or the public. Vendors and contributors are encouraged to contact us should any questions about this policy arise.

Our contact email is: (base64-encoded)

`Y29udGFjdEB2aWR5YXNlYy5vcmcgDQo=`

The PGP key associated with this account has the fingerprint:

`9F12 4D9F 059C B7AF E3B1 E4C1 AE15 9025 C9FA 2384`

The full key, also available at [/pubkey.asc](/pubkey.asc), is:

```text
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGNBGCwUjkBDACumMfLeAGA3uv/OvNPdRFc6cGMEE14G5N3bxI0phjyJ+LDZJXj
8erBFU9ZVSoA11U9mys0UXcCbhzjBUs/Twjx/+ZKHTGd9KdbLn897K9o66bBgDTc
d3f0AJBjDtOzjjvZXBILlAVl+U41/XOkltGaRBO6pmTE/nhWnoRTO+Z1GkrxeAud
hiFkfwQ7xrJUJvJ3ti76VRZpD4XU1dwGqglX72J/4DFAxEVAV4Q5bknyeQKr3Wu6
jaYTKoXjpXcqLNKe9ZcqyapRURBKo3afbsyrukr3+01B1umJPFS3wwsJ7p2iwa++
hEmvHsSFeqxZFlxxaR/z5cqDp9tqifCRBSdf9mqvw5c45l9U2LSC+ttKER9/Vcqk
TAFM+AeTcMNo89W9eLrnVifA8Ik9C35+FPdJmlHQNQhaC192cLsAlnDNBZGblnj/
Dr3xdTH/whEuCBaJT6WPZwYAmBNOEpP/PUjCy8Gp/J8GDBIv0IKZTdGMH/37w4BG
nwCclDnytGD/Pd8AEQEAAbQfVmlkeWFTRUMgPHZpZHlhc2VjQG91dGxvb2suY29t
PokB1AQTAQgAPhYhBJ8STZ8FnLev47Hkwa4VkCXJ+iOEBQJgsFI5AhsDBQkDwgD3
BQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJEK4VkCXJ+iOEMPcL/3AWD4zJ+WT4
doOODSXIdDZDM0CS+GUpedcYf3LhndneCuFRlCEZRWa9tlMgM1NrvDDNfGrlV1bh
vL0v/j11CJPaJpynglegIFtnBqSPutU7LfbvQM8svuV4ODPR1H6VUzQb8/kpll2W
AfPxw+Jio/QsSKZ6QFK+dIs2pb9IiB9dDJ6S7C9KSuPiEO+Pac2DPY3dt2A0yuNQ
RasFpOsJdngEOqmPykffGnPOmWiikRG2mggyTXDWsIMrGrfuSdOvp5pMF87IKvKM
8gBb4C6RYJ8tUNSGBLGXiLKXwpXvYJkS0w5Wyd2AE9s8vy7ZrRpfp7SsXjosOWEx
Jtu//OVfeLTIqz9z3szfINtOQx3MmqKN4ovidq2KKhrcAsqKM2UOVOa2lB8QMHzP
DREByOwz4hUowCANi9unwxhGUa4pLYaKgEgJxYKHikmOyeG3m29zMwt78xD3zQhH
zYXEXQJU7ZMxFeQcj3bLuqpDTu0zvZLwrmjYWl77LlB8fzqLsyHOC7kBjQRgsFI5
AQwA0ayAJkQ9zNzTp4STYzRUdKFpwdhGejlyKHYRen4G5Wz0e5C+pyHW5c02DBfJ
giphk5X1/IKIWx97X1ECozib9wCsM9ZRUtpfndxYUeCNI6HirEHYGxJQBhf01Mr5
5VNaU7EOgTDkHmVYZyW0k0AfRSI3qkFdttBunw6G3PbNp2wMNAKav2IlXwk6tVRK
pH/Q8Y4vRdOaQNdZ5fsAxO44X4M7NkmCcooyskjaGiuSWRGVxoKDeaMn5a1kk8Bz
9+ckGByvb7r1j2K30fewdGQRWLQ9uu3QMcD68yt6+skXYmjUGI3t9rOWtIWdtor9
YRuGe/YzHFmVedWOy0kQebxBTqpm8qk0La1kgNfcGLdcTtuNi3QQi6gfPdWajHlz
Br6TZJOZ3KcF/R/DQ5B4Bvwqr8DBmMMZXub5+SgagbCxSeCwKGGPGss/suSTeGR8
q1FSwPgmYFKfkhghaTMvAyMPMefNPq/TqvXNhm7o+kTI9Cnngny1uiJyV2scBXbb
IHU/ABEBAAGJAbwEGAEIACYWIQSfEk2fBZy3r+Ox5MGuFZAlyfojhAUCYLBSOQIb
DAUJA8IA9wAKCRCuFZAlyfojhEdAC/sFxKuZqSRX3PhjWwOvGN7zEK+tlZqYiZ6P
Yzdvv/jOYm2w5NKUTGWRx3kVW6vWeXWaGIjhm8jAT+KRZsFIB7ivxtRW7dryeijY
KJEoQ3gucMeaeP7bKWbSfJVAo4+yPct80h78Kps0Pbi0ZovSyo1JHw9kWS+Pp1Ke
MBCFsr/t5C5PHqsObl9s8CcGoJgKwyGrSoJITGIeKNDJzH6h+ZEY6d4HQSGA+3zR
NHmfMS6bqVrzJA4C5lRe4QyUTSfgom27IiGVazGMlw02DhTRC2BQ0biA1Ua3o1cE
NxjLkWO1aBJJq5Qj2tW7jawCPSeoJc6qghw1OTOOQaTvjoNYExd99eEB8IUi29HL
oUGwKq0YbgfZWdq9kq9f0d1BVP3Hj1skY1tSSdE+TTnh4zVJvGNdl4dbmvrBQj36
SZz7ZaeMav3F3004XruWgE89eagd0NzaIvXvcvXLrpe+IEQNY2NJywSoyyuZtsYy
iORjz5V93VtGMGNothu3XPhWaQgzQbw=
=1YID
-----END PGP PUBLIC KEY BLOCK-----
```